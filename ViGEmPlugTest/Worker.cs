﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Nefarius.ViGEm.Client;
using Nefarius.ViGEm.Client.Targets;
using Nefarius.ViGEm.Client.Targets.Xbox360;

namespace ViGEmPlugTest
{
    public class Worker
    {
        private Thread vbusThr;
        private ViGEmClient vigemTestClient = null;
        private IXbox360Controller outputX360 = null;

        public Worker()
        {
            // Change thread affinity. Create ViGEmClient instance used by Worker
            vbusThr = new Thread(() =>
            {
                vigemTestClient = new ViGEmClient();
            });

            vbusThr.Priority = ThreadPriority.AboveNormal;
            vbusThr.IsBackground = true;
            vbusThr.Start();
            vbusThr.Join(); // Wait for bus object start
        }

        public void Start()
        {
            // Delay before starting routine
            Thread.Sleep(2000);

            for(int i = 0; i < 100; i++)
            {
                Console.WriteLine($"ITER {i}");

                // Would probably be better to run as a Task rather than a new Thread
                Thread contThr = new Thread(() =>
                {
                    outputX360 = vigemTestClient.CreateXbox360Controller();
                    outputX360.AutoSubmitReport = false;
                    outputX360.Connect();
                });
                contThr.Priority = ThreadPriority.Normal;
                contThr.IsBackground = true;
                contThr.Start();
                contThr.Join(); // Wait for bus object start

                // Set A button down and transmit report
                outputX360.SetButtonState(Xbox360Button.A, true);
                outputX360.SubmitReport();

                // Need some delay or UserIndex won't be populated for some reason
                Thread.Sleep(100);

                // Uncomment code for ViGEmBus 1.17.333. Like to error out with 1.16.116 a lot for some reason
                Console.WriteLine($"USER INDEX: {outputX360.UserIndex}");

                // Disconnect virtual controller and set field to null
                outputX360?.Disconnect();
                outputX360 = null;

                // Add a short delay before plugging in a new virtual controller
                Thread.Sleep(10);
            }
        }
    }
}
